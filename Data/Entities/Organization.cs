﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Organization
    {
        public int Id { get; set; }
        public string Yearlystatistics { get; set; }
        public int LocationId { get; set; }
        public Location Location { get; set; }
        public string Longname { get; set; }
        public string Shortname { get; set; }
        public string Category { get; set; } // Should probably be typed?
        public string Homepage { get; set; }
        public string Subcategory { get; set; } // Should probably be typed?
        public int Supporters { get; set; }
        public long Points { get; set; }
        public int Rankingsupporter { get; set; }
        public int Rankingpoints { get; set; }
        public bool Issveaactive { get; set; }
        public bool ForcedInvalidation { get; set; }
        public bool CustomerHasAcknowledged { get; set; }
        public int Latestperiodamount { get; set; }
        public DateTime Latestperiodstartdate { get; set; }
        public DateTime Latestperiodenddate { get; set; }
        public DateTime Latestperiodcalcdate { get; set; }
        public DateTime Latestperiodpayouttime { get; set; }
    }
}
