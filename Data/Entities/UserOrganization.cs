﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class UserOrganization
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int OrganizationId { get; set; }
        public Organization Organization { get; set; }
    }
}
