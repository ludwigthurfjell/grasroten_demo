﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Location
    {
        public int LocationId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
    }
}
