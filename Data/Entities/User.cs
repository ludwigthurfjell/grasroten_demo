﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class User
    {
        public int UserId { get; set; }
        public IEnumerable<UserOrganization> Organizations { get; set; }
    }
}
