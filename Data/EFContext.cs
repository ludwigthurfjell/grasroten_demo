﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Data
{
   public class EFContext : DbContext
    {
        public EFContext(DbContextOptions<EFContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserOrganization>()
                .HasKey(bc => new { bc.UserId, bc.OrganizationId});
            modelBuilder.Entity<UserOrganization>()
                .HasOne(bc => bc.User)
                .WithMany(b => b.Organizations)
                .HasForeignKey(bc => bc.UserId);
        }

        public DbSet<Location>  Locations { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<UserOrganization> UserOrganizations { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
