﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public interface IAccess
    {
        EFContext Context { get; }
    }

    public class Access : IAccess
    {
        public EFContext Context { get; }
        public Access(EFContext eFContext)
        {
            Context = eFContext;
        }
    }
}
