﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    // Simple error type
    // TOOD: Add some language support eg
    public class ErrorDto
    {
        private ErrorDto(ErrorType errorType, string message)
        {
            ErrorType = errorType;
            Message = message;
        }

        public ErrorType ErrorType { get; }
        public string Message { get; }

        public static readonly ErrorDto TooManyUserOrganizations = new ErrorDto(ErrorType.MaxUserOrgs, "Ni har för många föreningar. Maximalt tre är tillåtet");
        public static readonly ErrorDto OrganizationMissing = new ErrorDto(ErrorType.OrgMissing, "Förening saknas");
    }

    public enum ErrorType
    {
        MaxUserOrgs = 1,
        OrgMissing = 2
    }
}
