﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    public class OrganizationCreateDto
    {
        public string SportName { get; set; }
        public string OrganizationName { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Homepage { get; set; }
    }
}
