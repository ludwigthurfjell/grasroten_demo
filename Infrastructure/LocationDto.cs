﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    public class LocationDto
    {
        public string Address { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
    }
}
