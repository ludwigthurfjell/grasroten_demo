﻿using Data;
using Data.Entities;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public interface IOrganizationsService
    {
        Task<Either<IEnumerable<OrganizationDto>, ErrorDto>> FetchOrganizationsByUserId(int userId);
        Task<Either<int, ErrorDto>> CreateOrganizationByUserId(OrganizationCreateDto organizationCreateDto, int loggedInUserId);
        Task<Either<int, ErrorDto>> DeleteUserOrganization(int organizationId, int loggedInUserId);
    }

    public class OrganizationsService : IOrganizationsService
    {
        private IAccess Access { get; set; }

        public OrganizationsService(IAccess access)
        {
            Access = access;
        }

        public async Task<Either<IEnumerable<OrganizationDto>, ErrorDto>> FetchOrganizationsByUserId(int userId)
        {
            var user = await Access.Context.Users
                .Include(x => x.Organizations).ThenInclude(x => x.Organization).ThenInclude(x => x.Location)
                .SingleAsync(u => u.UserId == userId);

            return user.Organizations.Select(x => x.Organization).Select(org => new OrganizationDto
            {
                Category = org.Category,
                Homepage = org.Homepage,
                Location = new LocationDto { Address = org.Location.Address, City = org.Location.City, Zipcode = org.Location.Zipcode },
                Id = org.Id,
                Points = org.Points,
                Shortname = org.Shortname,
                Subcategory = org.Subcategory,
                Supporters = org.Supporters
            })
        .ToList();
        }

        public async Task<Either<int, ErrorDto>> CreateOrganizationByUserId(OrganizationCreateDto dto, int loggedInUserId)
        {
            // Check if user has max organizations
            var numberOfUserOrganizations = await Access.Context.UserOrganizations.CountAsync(x => x.UserId == loggedInUserId);
            if (numberOfUserOrganizations >= 3) return ErrorDto.TooManyUserOrganizations;

            // Create new organization
            var location = await Access.Context.Locations.SingleOrDefaultAsync(x => x.Address == dto.Address && x.Zipcode == dto.ZipCode);
            var newOrganization = new Organization
            {
                Homepage = dto.Homepage,
                Location = location == null ? new Location { Address = dto.Address, City = dto.City, Zipcode = dto.ZipCode } : location,
                Subcategory = dto.SportName,
                Shortname = dto.OrganizationName,
                Longname = dto.OrganizationName
            };

            Access.Context.UserOrganizations.Add(new UserOrganization { UserId = loggedInUserId, Organization = newOrganization });
            return await Access.Context.SaveChangesAsync();
        }

        public async Task<Either<int, ErrorDto>> DeleteUserOrganization(int organizationId, int loggedInUserId)
        {
            var organization = await Access.Context.UserOrganizations.SingleOrDefaultAsync(x => x.OrganizationId == organizationId && x.UserId == loggedInUserId);
            if (organization == null) return ErrorDto.OrganizationMissing;

            Access.Context.UserOrganizations.Remove(organization);
            return await Access.Context.SaveChangesAsync();
        }
    }
}
