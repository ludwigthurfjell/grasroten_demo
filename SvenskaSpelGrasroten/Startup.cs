﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Data.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Services;

namespace SvenskaSpelGrasroten
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<EFContext>(options => options.UseInMemoryDatabase("InMem"));
            services.AddTransient<IAccess, Access>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.ConfigureCors();


            services.AddTransient<IOrganizationsService, OrganizationsService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IConfiguration configuration)
        {
            // Only taking barebones data that is used in the demo application
            var slimmedDownParse = configuration.GetSection("db:foreningar").GetChildren().ToList().Select(x => new Organization
            {
                Id = x.GetValue<int>("id"),
                Location = new Location
                {
                    Address = x.GetValue<string>("location:address"),
                    City = x.GetValue<string>("location:city"),
                    Zipcode = x.GetValue<string>("location:zipcode")
                },
                Shortname = x.GetValue<string>("shortname"),
                Longname = x.GetValue<string>("longname"),
                Category = x.GetValue<string>("category"),
                Homepage = x.GetValue<string>("homepage"),
                Subcategory = x.GetValue<string>("subcategory"),
                Points = x.GetValue<int>("points"),
                Supporters = x.GetValue<int>("supporters")

            });

            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                SeedSomeData(serviceScope.ServiceProvider.GetService<EFContext>(), slimmedDownParse);
            }


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().AllowCredentials());
            app.UseHttpsRedirection();
            app.UseMvc();

        }


        private static void SeedSomeData(EFContext context, IEnumerable<Organization> organizations)
        {
            context.Organizations.AddRange(organizations);
            context.Users.Add(new User { UserId = 1 });
            context.UserOrganizations.AddRange(new List<UserOrganization> { new UserOrganization { UserId = 1, OrganizationId = 26807 }, new UserOrganization { UserId = 1, OrganizationId = 10639 } });
            context.SaveChanges();
        }
    }
}
