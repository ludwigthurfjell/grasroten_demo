﻿using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SvenskaSpelGrasroten.Controllers
{
    public class OrganizationsController : BaseController
    {
        private readonly IOrganizationsService _organizationsService;
        public OrganizationsController(IOrganizationsService organizationsService)
        {
            _organizationsService = organizationsService;
        }

        // Hacky warmup for smoother development
        [HttpGet("warmup")]
        public IActionResult Warmup()
        {
            _organizationsService.FetchOrganizationsByUserId(LoggedInUserId);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetOrganizations()
        {
            return (await _organizationsService.FetchOrganizationsByUserId(LoggedInUserId)).Match<IActionResult>(
                orgs => Ok(orgs),
                error => BadRequest(error));
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrganization([FromForm] OrganizationCreateDto organizationCreateDto)
        {
            return (await _organizationsService.CreateOrganizationByUserId(organizationCreateDto, LoggedInUserId)).Match<IActionResult>(
                id => Ok(id),
                error => BadRequest(error));
        }

        [HttpDelete("{organizationId}/user")]
        public async Task<IActionResult> DeleteUserOrganization(int organizationId)
        {
            return (await _organizationsService.DeleteUserOrganization(organizationId, LoggedInUserId)).Match<IActionResult>(
                deletes => Ok(deletes),
                error => BadRequest(error));
        }
    }
}
